/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.abstractshape;

/**
 *
 * @author user
 */
public class TestShape {

    public static void main(String[] args) {
        Circle c1 = new Circle(5.2);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(1.5);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        
        Rectangle r1 = new Rectangle(4,3);
        Rectangle r2 = new Rectangle(3,2);
        System.out.println(r1);
        System.out.println(r2);
        
        Sqaure s1 = new Sqaure(4);
        Sqaure s2 = new Sqaure(2);
        System.out.println(s1);
        System.out.println(s2);
        
        System.out.println("--- All area of shape ---");
        Shape[] shape = {c1, c2, c3, r1, r2, s1, s2};
        for (int i = 0; i < shape.length; i++) {
            System.out.println(shape[i].toString() + " area : " + shape[i].calArea());
        }
    }

}
